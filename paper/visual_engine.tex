%\documentclass[journal]{vgtc}                % final (journal style)
%\documentclass[review,journal]{vgtc}         % review (journal style)
%\documentclass[widereview]{vgtc}             % wide-spaced review
\documentclass[preprint,journal]{vgtc}       % preprint (journal style)

%% Uncomment one of the lines above depending on where your paper is
%% in the conference process. ``review'' and ``widereview'' are for review
%% submission, ``preprint'' is for pre-publication, and the final version
%% doesn't use a specific qualifier.

%% Please use one of the ``review'' options in combination with the
%% assigned online id (see below) ONLY if your paper uses a double blind
%% review process. Some conferences, like IEEE Vis and InfoVis, have NOT
%% in the past.

%% Please note that the use of figures other than the optional teaser is not permitted on the first page
%% of the journal version.  Figures should begin on the second page and be
%% in CMYK or Grey scale format, otherwise, colour shifting may occur
%% during the printing process.  Papers submitted with figures other than the optional teaser on the
%% first page will be refused. Also, the teaser figure should only have the
%% width of the abstract as the template enforces it.

%% These few lines make a distinction between latex and pdflatex calls and they
%% bring in essential packages for graphics and font handling.
%% Note that due to the \DeclareGraphicsExtensions{} call it is no longer necessary
%% to provide the the path and extension of a graphics file:
%% \includegraphics{diamondrule} is completely sufficient.
%%
\ifpdf%                                % if we use pdflatex
  \pdfoutput=1\relax                   % create PDFs from pdfLaTeX
  \pdfcompresslevel=9                  % PDF Compression
  \pdfoptionpdfminorversion=7          % create PDF 1.7
  \ExecuteOptions{pdftex}
  \usepackage{graphicx}                % allow us to embed graphics files
  \DeclareGraphicsExtensions{.pdf,.png,.jpg,.jpeg} % for pdflatex we expect .pdf, .png, or .jpg files
\else%                                 % else we use pure latex
  \ExecuteOptions{dvips}
  \usepackage{graphicx}                % allow us to embed graphics files
  \DeclareGraphicsExtensions{.eps}     % for pure latex we expect eps files
\fi%

%% it is recommended to use ``\autoref{sec:bla}'' instead of ``Fig.~\ref{sec:bla}''
\graphicspath{{figures/}{pictures/}{images/}{./}} % where to search for the images

\usepackage{microtype}                 % use micro-typography (slightly more compact, better to read)
\PassOptionsToPackage{warn}{textcomp}  % to address font issues with \textrightarrow
\usepackage{textcomp}                  % use better special symbols
\usepackage{mathptmx}                  % use matching math font
\usepackage{times}                     % we use Times as the main font
\renewcommand*\ttdefault{txtt}         % a nicer typewriter font
\usepackage{cite}                      % needed to automatically sort the references
\usepackage{tabu}                      % only used for the table example
\usepackage{booktabs}                  % only used for the table example
\usepackage{amsmath}				   % used by argmax
%% We encourage the use of mathptmx for consistent usage of times font
%% throughout the proceedings. However, if you encounter conflicts
%% with other math-related packages, you may want to disable it.

%% In preprint mode you may define your own headline.
%\preprinttext{To appear in IEEE Transactions on Visualization and Computer Graphics.}

%% If you are submitting a paper to a conference for review with a double
%% blind reviewing process, please replace the value ``0'' below with your
%% OnlineID. Otherwise, you may safely leave it at ``0''.
\onlineid{0}

%% declare the category of your paper, only shown in review mode
\vgtccategory{Research}
%% please declare the paper type of your paper to help reviewers, only shown in review mode
%% choices:
%% * algorithm/technique
%% * application/design study
%% * evaluation
%% * system
%% * theory/model
\vgtcpapertype{system}

%% Paper title.
\title{VisualEngine: a Dynamic Visual-Augmented Web Search Engine with Query Refinement Feedback Mechanism}

%% This is how authors are specified in the journal style

%% indicate IEEE Member or Student Member in form indicated below
\author{Gabriele Proietti Mattia, Alexandru Daniel Tufa}
\authorfooter{
%% insert punctuation at end of each item
\item 
  Gabriele Proietti Mattia from Sapienza University of Rome. E-mail: pm.gabriele@gmail.com.
\item
 Alexandru Daniel Tufa from Sapienza University of Rome. E-mail: alex.tufa94@gmail.com.
}

%other entries to be set up for journal
\shortauthortitle{Biv \MakeLowercase{\textit{et al.}}: Global Illumination for Fun and Profit}
%\shortauthortitle{Firstauthor \MakeLowercase{\textit{et al.}}: Paper Title}

%% Abstract section.
\abstract{One of the most frequent issues when using a web search engine is ambiguity: the property of being open to more than one interpretation. Often the user/query intent, when it is too general, is not satisfied by the search engine, given by the fact that the system does not receive the input needed for yielding such expected results. This results in an output that can deviate from what was originally wanted. The user may also not have a clear depiction of what he is searching for and this can further increase the ambiguity of the request. Several proposals have been made to address this~\cite{Ashkan2009ClassifyingAC}~\cite{Hashemi2016QueryID} that, through the use of machine learning (SVM and CNN), try to detect, classify and characterize query intention. To aid the user when using a web search engine, we propose a visual analytics system called VisualEngine that allows him to explore and deepen the understanding of the topics and the relationships between pages and their relevance with respect to the provided query. We believe that our system not only helps the user in reaching faster and easier his goal, but also offers a better knowledge on how the provided results are connected semantically and physically, given by the intrinsic linked structure of the web.%
} % end of abstract

%% Keywords that describe your work. Will show as 'Index Terms' in journal
%% please capitalize first letter and insert punctuation after last keyword
\keywords{Text analytics, visual analytics, web search engine, text classification, concepts}

%% ACM Computing Classification System (CCS). 
%% See <http://www.acm.org/class/1998/> for details.
%% The ``\CCScat'' command takes four arguments.

\CCScatlist{ % not used in journal version
 \CCScat{K.6.1}{Management of Computing and Information Systems}%
{Project and People Management}{Life Cycle};
 \CCScat{K.7.m}{The Computing Profession}{Miscellaneous}{Ethics}
}

%% Uncomment below to include a teaser figure.
\teaser{
  \centering
  \includegraphics[width=\linewidth]{VisualEngineTeaser}
  \caption{VisualEngine allows to understand the intrinsic physical and logical nature of the results thanks to different visualizations. On the left (\textrm{I}) there is the visual interface that is firstly presented to the user after pressing the search button. In this area, the system offers to the user a representation of the linked structure of the results (1) and a depiction of the main concepts behind the web pages and their belonging (2). Documents are also presented through the use of two scatter plots where dimensionality reduction is applied using PCA (3) and t-SNE (4). To have a correlation between the outcome of the query, represented as a list, and the mentioned charts, a grid (5) is used. On the right (\textrm{II}) there is a second visual interface that is accessible by scrolling the web page. This area offers the classic results list and it allows their further analysis by relying on the radar chart (6) that plots the score that the selected results have for every concept.}
	\label{fig:teaser}
}

%% Uncomment below to disable the manuscript note
%\renewcommand{\manuscriptnotetxt}{}

%% Copyright space is enabled by default as required by guidelines.
%% It is disabled by the 'review' option or via the following command:
% \nocopyrightspace

\vgtcinsertpkg

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%% START OF THE PAPER %%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

%% The ``\maketitle'' command must be the first command after the
%% ``\begin{document}'' command. It prepares and prints the title block.

%% the only exception to this rule is the \firstsection command
\firstsection{Introduction}

\maketitle

We live in a world in which the amount of produced data is higher than it has ever been, meaning that the variety and nuance a certain argument can achieve is endless. The power of language is such that with different terms we can define the same concept and also by using the same word we can describe a multitude of notions. For example, the word \textit{jaguar} could refer to the car, to the animal or to the Olmec deity. Being able to understand and personalize each query to a specific user is not an easy task and the methods for addressing this issue are divided in two classes: global methods and local methods~\cite{Manning:2008:IIR:1394399}\cite{Xu1996QueryEU}. The first ones do not take into account the relationship between the query results and the query itself since they are only based on helping the user in constructing the query, for example we could use spelling correction for fixing misspelled word or we could do even better by suggesting terms to the user by using a pre-defined thesaurus or automatically generating a dictionary from the corpus~\cite{Qiu1993ConceptBQ}\cite{Schtze1997ACT}. Local methods, instead, follow a totally different approach since they are based on the feedback given by the user to the results of a query. In this context, we firstly need to give some results to the user and then we should make him able to decide what results are fitting for his intent or not, in particular we ask him to mark which result are relevant or non-relevant with respect to the query. All of these methods, whose main representative is~\cite{Roc71}, are called~\textit{relevance feedback} methods and they can be used in many creative ways. For example, we could automatically mark all the top 10 results as relevant (pseudo-relevance feedback) or we also could improve the precision of the refinement by only considering significant terms in the marked relevant documents~\cite{Lv2010PositionalRM}.

What is the most efficient way for the user to select which results are relevant and which not? How we can help him in understanding the nature of the results? From here VisualEngine was born, giving to the user the most comprehensive depiction of the results to the query and allowing the user to consciously select which results are relevant or not. Once the relevance feedback is so built, the system can re-run the search algorithm.

When creating VisualEngine we started from relevance feedback and expanded the idea of interacting with single results to get better ones. Instead of selecting each page one at a time, the user has the possibility to select a cluster of pages as not relevant to his request. The used charts not only give this opportunity, but also allow for a better understanding of how each page is related to other ones. For analyzing single pages, the system provides a radar chart next to the results to interact with them and see what is the percentage of each topic in single documents and confront them.


\section{Related Work}
\subsection{Relevance Feedback}
One of the most important and significant features of VisualEngine is using it as a way to get better and more results regarding a certain topic. This idea is in line with the one provided by relevance feedback and it is on this notion that our system is built on. Relevance feedback is based on the involvement of the user in the information retrieval process so as to improve the final result set. Unlike VisualEngine, the user gives feedback on the relevance of single documents. A good example of relevance feedback is given by image search, where it is easy to analyze in a fast way the returned results. Newsam et al.~\cite{Newsam2001CategorybasedIR} developed this kind of refinement approach: by inserting an initial query, as for example \textit{bike}, the initial image results are returned, and we could have for example: \textit{motorbikes}, \textit{bikes}, \textit{bike magazines}. That system allowed the user to select which are the images the user was interested in and according to them the results were refined. This is precisely what inspired us in creating VisualEngine, a system which gives more instruments and information towards the query refinement process.

The algorithm used for implementing relevance feedback, which is also used by our system, is the Rocchio algorithm~\cite{Roc71}, or a slightly modified version which is the SMART algorithm. Our ultimate objective is to find a query vector, denoted as $\vec{q}$, that maximizes similarity with relevant documents while minimizing similarity with nonrelevant documents. If $C_r$ is the set of relevant documents and $C_{nr}$ is the set of nonrelevant documents, then we wish to find:
$$\vec{q}_{opt}=\underset{\vec{q}}{\textrm{argmax}}[\textrm{sim}(\vec{q},C_r) - \textrm{sim}(\vec{q},C_{nr})]$$
If as similarity measure we use cosine similarity, then the optimal vector becomes:
$$\vec{q}_{opt}=\frac{1}{|C_r|}\sum_{\vec{d}_j \in C_r}\vec{d}_j - \frac{1}{|C_{nr}|}\sum_{\vec{d}_j \in C_{nr}}\vec{d}_j$$
To better tweak this values, our system uses the SMART algorithm which adds weights attached to each term and also considers the previous inserted query:
$$\vec{q}_{m}=\alpha\vec{q}_0 + \beta\frac{1}{|C_r|}\sum_{\vec{d}_j \in C_r}\vec{d}_j - \gamma\frac{1}{|C_{nr}|}\sum_{\vec{d}_j \in C_{nr}}\vec{d}_j$$

\subsection{Visualization-based Information Retrieval on the Web}

A relevant study about visualization-based information retrieval was given by Koshman~\cite{Koshman2006VisualizationbasedIR}. Its focus is on the ``\textit{theoretical constructs applied to interpreting a graphical display, the icons associated with document representation and the tasks associated with user research into information visualization systems}''. One set of theoretical principles are the Gestalt laws or principles of grouping which state that humans naturally perceive objects as organized patterns and objects. They are: \textit{proximity}, according to which the user perceives as inherently correlated objects that appear close together; \textit{closure}, that regards perception of boundaries; \textit{continuity}, based on the perception of continuous attributes as mean for recognizing correlated objects. An example of the Gestalt law of continuity is given by the \textit{Kartoo} visualization metasearch engine that links a clicked result with related ones. Another important aspect treated by Koshman is that of dimensionality and the relationship between the dimensionality associated with positioning objects in the information display and the dimensionality of the document object. For example, \textit{Grokker} is a web-based system that uses two-dimensional displays, even if iconic positioning is not relevant. Icon color and size are used to depict clusters of topics in circular patterns, but these items are not depicted in relation to the x-y coordinates. Last but not least, an intuitive way of visualizing the results is offered by \textit{TouchGraph}. Its interface shows the node containing the query term located near the center of the display with lines connecting related nodes. By interacting with these nodes we can expand them to reveal additional related nodes. This chart allows for adjusting the number of node links, thus giving the user the possibility to decide how many elements he wants displayed.

An interesting survey about these \textit{clustering search engines} was made by Carpineto et al.~\cite{Carpineto2009ASO}.


\section{Motivation}
By using a normal search engine we perceived that it was mostly meant for searching single elements in a fast way. While this proves to be good for that reason, we wanted something different. We wanted something that allowed searching not for single results, but for single topics; something that provides knowledge on how the results are connected between them logically and, why not, physically. A page that is linked by other websites with the same argument has different relevance from a page referenced by sites with a different topic. Being able to see what is the underlying concept behind a certain document without the need to inspect it results in a faster and less tedious user experience. While other methods, such as relevance feedback, give the possibility to refine the results by inspecting single elements, we wanted a system capable of doing this in a faster and intelligent way. Ambiguous queries usually produce batches of semantically related results and being able to remove one cluster and give more relevance to the others allows in further reaching the user intent. These motivations drove us in creating VisualEngine, a system built on a classic web search engine, but supplemented by visualizations that aid and guide the user in better understanding what he truly demands.

\section{VisualEngine System}
The VisualEngine system was built focusing the following design rationale:

\begin{enumerate}
	\item \textbf{Offering a basic web search engine system}. This is the starting point of the entire system, and this is achieved by using cosine similarity upon a TF-IDF scoring model~\cite{Ramos1999} for the pages.
	\item \textbf{Visualizing how the results are physically and logically linked}. VisualEngine offers six different visualization that are aimed to represent:
	      \begin{enumerate}
		      \item the physical structure of the pages, with the WebGraph (1)
		      \item the logical position of a page, with the Concept Chart (2), the PCA Chart (3) and t-SNE Chart (4)
		      \item the physical position of a page among the results with the Grid Chart (5)
		      \item the scoring of each page with respect to every concept, with the Radar Chart (6)
	      \end{enumerate}
	\item \textbf{Supporting query refinement by allowing the user to choose which pages are not related to the query}. This is achieved by allowing the user to select which concept he wants to mark as non-relevant and as a consequence all the pages in that concept, otherwise the user can brush on scatter plots and choose to mark as non-relevant selected pages by using the context menu. The marking action automatically triggers the Rocchio algorithm~\cite{Roc71} for performing the query refinement.
\end{enumerate}

In the following sections we provide a walkthrough among the techniques that we used for achieving the rationale guidelines both by the front-end and the back-end points of view.

\subsection{Front-end and visual interface}
Front-end has been realized by using AngularJS\footnote{\url{https://angularjs.org}} as a javascript framework, and this allowed us to build a single-page web application that is completely downloaded by the browser the first time. Then the requests made to the back-end server provide only the raw-data for building the front-end charts, realized with the library d3js.

The starting screen of the system is a simple search bar, as in a classic search engine. When the user presses the search button the complete interface is displayed. This visual interface is essentially divided in two parts. The upper part (I) offers five visualizations:
\begin{enumerate}
	\item\label{chart:web-graph} The \textbf{Web Graph}. Represents the graph of the web composed by the result pages. In this visualization, the color of the nodes encodes the concept to which they belong to and their size encodes their PageRank score~\cite{ilprints422}.
	\item\label{chart:concepts} The \textbf{Concept Chart} is a RadViz~\cite{Novkov2006MultidimensionalCI} that represents pages colored according to their predominant concept color (namely the concept that has the highest score for that page) and the strength of every link, that keeps the node in the chart, encodes the score of the corresponding concept.
	\item\label{chart:pca} The \textbf{PCA Chart} is a scatter plot that represents the output of the first 2 dimensions of the PCA algorithm~\cite{Jolliffe2002PCA} applied to our TF-IDF representation of the pages.
	\item\label{chart:tsne} The \textbf{t-SNE Chart} is a scatter plot that represents the output of 2-dimensions t-SNE~\cite{vanDerMaaten2008} applied to our TF-IDF representation of the pages.
	\item\label{chart:grid} The \textbf{Grid Chart} represents every result page as a colored box, again the color encodes the concept to which the page belongs. The boxes are sorted according to the order of the resuts, starting from the top-left corner, in which we have the first result to the bottom-right corner, in which we have the last result.
\end{enumerate}

The purpose of this part is to allow the user to have a complete view of the results to his query which he can interact with in different ways:
\begin{itemize}
	\item by positioning the pointer on a page representation, a tooltip is shown with all the information about that page as the title, the description, its position in the results list and the concept it belongs to;
  \item by left-clicking on the concept representation of the Concept Chart (\ref{chart:concepts}), the pages-concept are shown. We call \textit{pages-concept} the pages that have the highest score for that concept, and as a consequence they are the best representation of a particular concept;
  \item by right-clicking on the concept representation of the Concept Chart (\ref{chart:concepts}), the user can decide to mark that concept cluster as non-relevant. Upon this action the results of the query and all charts are reloaded as a new query has been provided.
  \item by brushing on PCA Chart (\ref{chart:pca}) or on the t-SNE (\ref{chart:pca}) Chart all selected pages are highlighted in every other chart, moreover the user can right-click on the brush area to mark the selected elements as non-relevant and rerun the search algorithm. 
\end{itemize}

An important fact that we have to notice, with respect to the query refinement mechanism, is that since the Rocchio algorithm used in the back-end just modifies the query vector, according to the user feedback, this does not mean that all the pages belonging to the selected pages (or all the pages related to the selected concept) are removed from the results, this is not the purpose of the query feedback. Indeed the marking of a page as non-relevant means that the center of mass of the relevant pages is changed, the query vector is moved and the cosine similarity is run again with this new query vector~\cite{Roc71}

The lower part (II) of the interface contains the results of the query and another visualization, the \textbf{Radar Chart} (6). The user can again interact with the results: by selecting result pages in the list, the Radar Chart allows to visualize the scores of each page with respect each concept generated by the system.

\subsection{Back-end server}
The entire back-end was written in Python with the use of Falcon\footnote{\url{https://falconframework.org/}} as web API framework. This framework encourages the REST architectural style and allows for building very fast application back-ends, giving the possibility for concentrating more on the actual functionality and less on the boilerplate. The used crawler is Scrapy\footnote{\url{https://scrapy.org/}}, an open source and collaborative crawling framework for extracting the needed data from websites. It allows saving the data directly on our chosen database, which is mongoDB\footnote{\url{https://www.mongodb.com/}}. By storing the information regarding web pages as JSON objects directly on this NoSQL database, we did not need to create a parser for converting database objects into JSON objects, which is the format used by our web framework to transfer data to the front-end. The main library used by the system for data analysis is scikit-learn\footnote{\url{http://scikit-learn.org/stable/}}. It allowed us to perform in a simple and efficient way: TF/IDF, cosine similarity, Singular Value Decomposition (SVD)~\cite{Eckart1936}, Principal Component Analysis~\cite{Jolliffe2002PCA} and t-distributed Stochastic Neighbor Embedding~\cite{vanDerMaaten2008}. For generating the weights in the web graph, the system computes PageRank~\cite{ilprints422} using NetworkX\footnote{https://networkx.github.io/}, a Python package for the creation, manipulation and study of the structure, dynamics and functions of complex networks.

\subsection{Web Search Engine}
VisualEngine is built upon a web search engine. In order to achieve it, we do the required steps:
\begin{itemize}
	\item use a crawler to get a document corpus
	\item compute a score for each document
	\item give a score to the entered query and compare it to the scores of each document
	\item return the pages whose score is higher
\end{itemize}

Because of the restrictions given by space and time, a crawl on the Wikipedia website was made. For each page, the crawler took title, url, the first 500 characters, all outgoing links and the hash of the url to be used for indexing.

To compute the score for each document  the system uses TF/IDF scoring model. For each term in a page we compute a weight which is given by its frequency and inverse document frequency. This value increases proportionally to the number of times the word appears in the document and is inversely proportional to the frequency of the documents that contain it. By grouping together all these weights corresponding to the words in the document we can form and assign a vector to each page. This reasoning is also applied to the inserted query and gives us the possibility to compare it to the corpus. As criteria of comparison the system uses cosine similarity which is a measure of similarity between two non-zero vectors of an inner product space that measures the cosine of the angle between them. We use this measure and not the Euclidean distance because it is independent of the length of the vector. The order of the returned documents reflects the score given by the cosine similarity measure: higher the similarity, higher the position.

\subsection{Charts Description}
\paragraph{Web Graph} Each chart exploits the information collected during the crawling phase or the data offered by the results of each query. Because the crawler saved, for each page, its outgoing links, the system can represent documents as nodes and links as edges, hence the construction of the Web Graph~(\ref{chart:web-graph}). In this way it can display to the user how the results are connected between them. To better visualize important nodes, we assign them a weight with respect to their PageRank~\cite{ilprints422} score. To determine how important a document is we count the number and quality of links to it, thus giving a rough estimate of its importance.

\paragraph{Concepts Chart} In order to assign each document to a certain topic, Singular Value Decomposition (SVD)~\cite{Eckart1936} is used. This dimensionality reduction technique allows for reducing the number of features, which in our case is the TF/IDF score assigned to each word, while maintaining the number of samples, which corresponds to the returned results of the query. The use of term-frequency/inverse-document-frequency together with Singular Value Decomposition is often called Latent Semantic Analysis, which is a technique that assumes that words that are close in meaning will occur in similar pieces of text. The resulting scores for each document are passed to the front-end of the system, resulting in the Concepts Chart~(\ref{chart:concepts}). To get a grasp of what each concept refers to, the back-end also offers the documents with highest scores for each one.

\paragraph{Scatter Plots} This data is used furthermore to classify the points in the PCA Chart~(\ref{chart:pca}) and in the t-SNE Chart~(\ref{chart:tsne}), which are obtained through dimensionality reduction techniques, such as Principal Component Analysis (PCA)~\cite{Jolliffe2002PCA} and t-Distributed Stochastic Neighbor Embedding (t-SNE)~\cite{vanDerMaaten2008}. The former is a technique that transforms the original vectors into a new coordinate system such that, by doing the projection of the data, the greatest variance comes to lie on the first coordinate, which is called the first principal component, the second greatest variance on the second coordinate, and so on. For our bi-dimensional representation we use the first and second principal component. The t-SNE visualization is based on a nonlinear dimensionality reduction technique. This approach builds a probability distribution over pairs of high-dimensional objects in such a way that similar objects have a high probability of being picked, whilst dissimilar points have an extremely small probability of being picked. After defining a similar probability distribution over the points in the low-dimensional map, it minimizes the Kullback-Leibler divergence between the two distributions with respect to the locations of the points in the map.

\section{Evaluation}
To evaluate the classification given by our system, we used the categories offered by DBpedia\footnote{\url{https://wiki.dbpedia.org/}} for each Wikipedia page. Since VisualEngine considers a concept to be defined by its 3 most significant pages (which are the pages that have the maximum score for that particular concept), according to the SVD weighting, we grouped together all DBpedia categories regarding those pages. We consider a document to be correctly classified if at least one of its categories assigned by DBpedia is present inside the category set of the concept. To do this a Python script has been built that exploits the information collected during the scraping of the corpus (see \autoref{tab:evaluation}).

\begin{table}[tb]
	\caption{Percentages of correctly classified pages given the query and the concept number. Missing percentages mean that no page belonged to that concept and zero percent mean that all the pages that represent the concept has no DBpedia categories and this is possible for disambiguation pages.}
	\label{tab:evaluation}
	\scriptsize%
	\centering
	\begin{tabular}{llllll}
		\toprule
		                           & \textbf{\#0} & \textbf{\#1}          & \textbf{\#2}          & \textbf{\#3}          & \textbf{\#4}          \\
		\midrule
		\textit{Quantum of Solace} & 58.95\%      & 89.47\%               & \multicolumn{1}{c}{-} & \multicolumn{1}{c}{-} & \multicolumn{1}{c}{-} \\
		\textit{Jaguar Speed}      & 0\%          & 100\%                 & 100\%                 & 6.67\%                & 100\%                 \\
		\textit{Quantum}           & 56.36\%      & \multicolumn{1}{c}{-} & 100\%                 & 100\%                 & 100\%                 \\
		\textit{Jaguar cat}        & 6.50\%       & 37.83\%               & 100\%                 & 100\%                 & 100\%                 \\
		\bottomrule
	\end{tabular}
\end{table}

The domain of the scraping influences a lot the evaluation of the system, as a consequence we chose two topics that can offer disambiguation: ``\textit{jaguar}'' and ``\textit{quantum}''. For this reason we scraped Wikipedia, in a BFS fashion, choosing as starting pages ``\textit{Quantum (disambiguation)}''\footnote{\url{https://en.wikipedia.org/wiki/Quantum\_(disambiguation)}} and ``\textit{Jaguar (disambiguation)}''\footnote{\url{https://en.wikipedia.org/wiki/Jaguar\_(disambiguation)}}. We collected about 15’000 pages and we analyzed how the system behaved with the following queries:
\begin{itemize}
	\item \textit{Quantum of solace} - (\autoref{fig:query_quantum_of_solace_charts}) with this query we saw almost perfect results both in the concept clustering and in the output of the charts; the web graph was perfectly divided showing how related topics are physically linked and also the scatter plots formed clusters totally divided.
	\item \textit{Jaguar speed} - (\autoref{fig:query_jaguar_speed_charts}) this query provided mixed results. While our query intention was to retrieve something related to both the animal and car speeds, our system focused mostly on cars and on speed in general. The main clusters are: \textit{Jaguar XK} which is a specific car, \textit{Jaguar Land Rover}, supersonic speed and a cluster that groups general diverse things. The most distinguishable information provided here is given by the arrangement of two different clusters, where the first one focuses on the car aspects while the second one on the speed ones.
	\item \textit{Quantum} - (\autoref{fig:query_quantum_charts}) given that our scraper uses the disambiguation page of this argument as starting point, it will retrieve many relevant pages regarding this argument. For this reason it generates a main cluster that contains the results.
	\item \textit{Jaguar cat} - (\autoref{fig:query_jaguar_cat_charts}) this query gives us a pretty good outcome where two clusters are generated. The first one focuses on Jaguar cars, while the latter on cats. Other small chunks focus on singular elements, for example a particular car \textit{Jaguar XK}. The curious thing that we noted was how the system added also some pages regarding quantum mechanics, probably related because of Schrödinger's cat.
\end{itemize}

\begin{figure}[tb]
	\centering
	\includegraphics[width=\columnwidth]{query_quantum_of_solace_charts}
	\caption{Charts for the ``\textit{Quantum of Solace}'' query}
	\label{fig:query_quantum_of_solace_charts}
\end{figure}

\begin{figure}[tb]
	\centering
	\includegraphics[width=\columnwidth]{query_jaguar_speed_charts}
	\caption{Charts for the ``\textit{Jaguar Speed}'' query}
	\label{fig:query_jaguar_speed_charts}
\end{figure}

\begin{figure}[tb]
	\centering
	\includegraphics[width=\columnwidth]{query_quantum_charts}
	\caption{Charts for the ``\textit{Quantum}'' query}
	\label{fig:query_quantum_charts}
\end{figure}

\begin{figure}[tb]
	\centering
	\includegraphics[width=\columnwidth]{query_jaguar_cat_charts}
	\caption{Charts for the ``\textit{Jaguar Cat}'' query}
	\label{fig:query_jaguar_cat_charts}
\end{figure}

\section{Conclusion and future work}
The most relevant obstacle we noted during our work was given by the computation power offered by our machines. The size of the dataset proved to be a sensible thing to work with, allowing us to only show 150 results to the user instead of the initial 500 that we tried. By doing some trials we saw that the number of links between pages was proportional to the number of crawled pages; this constrained us in taking only a limited number of links for each page. To counter this, PageRank~\cite{ilprints422} was used that, even if we removed some links, still displayed the relevance of single nodes based on the number of links and their respective importance. 

Another non trivial aspect that we encountered was the recall being highly influenced by the fact that our system could not handle correctly \textit{query proximity}~\cite{edseee.529183520090101}\cite{Croft1991TheUO}\cite{Panev2014PhraseQW}\cite{Bahle2002EfficientPQ}. To be able to deal with this issue meant modifying the actual implementation of TF/IDF, but this implied using an algorithm not as fast and efficient as the one provided by the used library. We experimented this manual approach, but with just a few documents the needed time was high. We believe this problem can be solved efficiently by using a MapReduce methodology and dividing the work among many workers in parallel.

%% if specified like this the section will be committed in review mode
\acknowledgments{
	The authors wish to thank Sapienza University of Rome. This work was supported by professor Giuseppe Santucci and Marco Angelini of the Visual Analytics course and Andrea Vitaletti and Luca Becchetti of the Web Information Retrieval course.
}

%\bibliographystyle{abbrv}
%\bibliographystyle{abbrv-doi}
%\bibliographystyle{abbrv-doi-narrow}
%\bibliographystyle{abbrv-doi-hyperref}
\bibliographystyle{abbrv-doi-hyperref-narrow}

\bibliography{visual_engine}
\end{document}

