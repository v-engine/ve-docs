# This script is used for evaluating the search engine clustering
#
# Copyright (C) 2018 Gabriele Proietti Mattia <gabry.gabry@hotmail.it>; Alexandru Daniel Tufa <alex.tufa94@gmail.com>
# 
# This file is part of evaluation_script.
# 
# evaluation_script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# evaluation_script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with evaluation_script.  If not, see <http://www.gnu.org/licenses/>.
# 

import sys
import requests
import json
from db import Database

API_URL = "http://localhost:8000/"
DEBUG = False


def main(query):
    query_hash = apiGetQuery(query)
    concept_data = apiGetConceptData(query_hash)
    concept_classes = computeConceptPagesCategories(concept_data)
    percentages = do_experiment(concept_data, concept_classes)
    print("\n==> Percentages result")
    print(percentages)


def apiGetQuery(query):
    '''Make get and return the hash'''
    response = requests.get(API_URL + "query?q=" + query)
    return response.json()["hash"]


def apiGetConceptData(query_hash):
    # get how much pages
    response = requests.get(API_URL + "concept_graph/" + query_hash + "/0")
    concept_data = response.json()
    pages = response.json()["pages"]
    for i in range(1, pages):
        response = requests.get(
            API_URL + "concept_graph/" + query_hash + "/" + str(i))
        concept_data["data"] += response.json()["data"]
    return concept_data


def dbGetDbpediaCategoriesForHash(url_hash):
    db = Database.getInstance()
    page = db.pages_collection.find_one({"url_hash": url_hash})
    if page == None:
        print("Page not found!")
    return page["categories"]


def computeConceptPagesCategories(concept_data):
    classes = []
    cluster_index = 0
    for dim in concept_data["dimensions"]:
        concept_classes = set()
        for concept_page in dim:
            concept_classes = concept_classes.union(
                set(dbGetDbpediaCategoriesForHash(concept_page)))
        classes.append(list(concept_classes))
        # check if cluster has classes
        if len(concept_classes) == 0:
            print("Cluster#" + str(cluster_index) + " has no classes! Results will not be correct!")
        cluster_index += 1
    return classes


def do_experiment(concept_data, concept_classes):
    correct = [0 for i in concept_classes]
    pages_per_class = [0 for i in concept_classes]
    for page in concept_data["data"]:
        page_classes = dbGetDbpediaCategoriesForHash(page["url_hash"])
        # skip no category pages
        if len(page_classes) == 0:
            continue
        cluster_index = int(page["cluster_index"])
        pages_per_class[cluster_index] += 1

        misclassified = True
        for page_class in page_classes:
            if page_class in concept_classes[cluster_index]:
                correct[cluster_index] += 1
                misclassified = False
                break

        if misclassified and DEBUG:
            print("==> Misclassified")
            print(page["title"])
            print(page["url_hash"])
            print(page_classes)
            print(concept_classes[cluster_index])
            print("\n")

    percentages = []

    for i in range(len(correct)):
        if pages_per_class[i] != 0:
            percentages.append(correct[i] / pages_per_class[i])
        else:
            percentages.append("N/A")

    return percentages

# usage `python clustering_exp.py "query text"`
if __name__ == "__main__":
    if len(sys.argv) == 2:
        main(sys.argv[1])
    else:
        print("Usage: python clustering_exp.py \"query text\"")