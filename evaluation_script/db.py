# Copyright (C) 2018 Gabriele Proietti Mattia <gabry.gabry@hotmail.it>; Alexandru Daniel Tufa <alex.tufa94@gmail.com>
# 
# This file is part of evaluation_script.
# 
# evaluation_script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# evaluation_script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with evaluation_script.  If not, see <http://www.gnu.org/licenses/>.
# 

import pymongo


class Database:

    MONGODB_SERVER = "localhost"
    MONGODB_PORT = 27017
    MONGODB_DB = "v-engine"
    MONGODB_PAGES_COLLECTION = "pages"
    MONGODB_BIN_DATA_COLLECTION = "binary-data"
    MONGODB_QUERY_COLLECTION = "queries"
    __instance = None

    def __init__(self):
        if(Database.__instance is not None):
            raise Exception("This class is a singleton")
        connection = pymongo.MongoClient(
            self.MONGODB_SERVER,
            self.MONGODB_PORT
        )
        db = connection[self.MONGODB_DB]
        self.pages_collection = db[self.MONGODB_PAGES_COLLECTION]
        self.bin_data_collection = db[self.MONGODB_BIN_DATA_COLLECTION]
        self.queries_collection = db[self.MONGODB_QUERY_COLLECTION]

    @staticmethod
    def getInstance():
        if(Database.__instance is None):
            Database.__instance = Database()
        return Database.__instance
