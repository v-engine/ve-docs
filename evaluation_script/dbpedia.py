# This script downloads dbpedia categories for pages that have not them 
#
# Copyright (C) 2018 Gabriele Proietti Mattia <gabry.gabry@hotmail.it>; Alexandru Daniel Tufa <alex.tufa94@gmail.com>
# 
# This file is part of evaluation_script.
# 
# evaluation_script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# evaluation_script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with evaluation_script.  If not, see <http://www.gnu.org/licenses/>.
# 

from SPARQLWrapper import SPARQLWrapper, JSON
from db import Database


def main():
    updateDatabaseCategories()
    return


def updateDatabaseCategories():
    db = Database.getInstance()
    pages = db.pages_collection.find({'categories': []})
    pages_count = pages.count()
    print("=> Pages without a category: " + str(pages_count))

    index = 1
    for page in pages:
        print("=> Working on page " + str(index) + "/" + str(pages_count))
        index += 1
        categories = getCategories(page['url'][30:])
        if(len(categories) == 0):
            print("For " + page['url'][30:] + " there are not categories")
            continue

        db.pages_collection.update_one(
            {
                'url_hash': page['url_hash'],
            },
            {
                '$set': {
                    'categories': categories
                }
            },
            upsert=True
        )

    print("=> Correctly updated categories")

    pages = db.pages_collection.find({'categories': []})
    print("=> Pages without a category: " + str(pages.count()))


def getCategories(resource):
    # do a SPARQL request to DBpedia to get the categories
    sparql = SPARQLWrapper("http://dbpedia.org/sparql")
    sparql.setQuery("""
        SELECT *
        WHERE {<http://dbpedia.org/resource/""" + resource + """> <http://purl.org/dc/terms/subject> ?categories .}
    """)
    sparql.setReturnFormat(JSON)
    result = sparql.query().convert()

    categories = []
    for res in result['results']['bindings']:
        categories.append(res['categories']['value'][37:])

    return categories


main()
